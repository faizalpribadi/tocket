package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"

	"github.com/Shopify/sarama"
	"github.com/faizalpribadi/api/event"
	"github.com/faizalpribadi/api/socket/client"
	"github.com/faizalpribadi/api/socket/hub"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
)

func consumerListener() {
	log.Println("Worker Consumer is started and connected")
	// sarama.Logger = log.New(os.Stdout, "", log.Ltime)

	uid, _ := uuid.NewV4()
	config := sarama.NewConfig()
	config.ClientID = "INFONESIA-WORKER-" + uid.String()
	config.Consumer.Return.Errors = true

	master, err := sarama.NewConsumer([]string{"localhost:9092"}, config)
	if err != nil {
		panic(err)
	}

	defer func() {
		if err := master.Close(); err != nil {
			panic(err)
		}
	}()

	consumer, err := master.ConsumePartition("testing", 0, sarama.OffsetNewest)
	if err != nil {
		panic(err)
	}

	messageCountStart := 0
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt)
	doneCh := make(chan struct{})

	go func() {
		for {
			select {
			case err := <-consumer.Errors():
				fmt.Println(err)

			case msg := <-consumer.Messages():
				// chClient := make(chan client.Client)
				hub := hub.NewHub()
				messageCountStart++
				event := &event.Event{}
				err := json.Unmarshal(msg.Value, &event)
				if err != nil {
					panic(err)
				}
				if hub.HasClient(event.UserID) {
					c, _ := hub.GetClient(event.UserID)
					c.Send([]byte(msg.Value))

					log.Println(msg)
				} else {
					return
				}
				fmt.Println("Received messages", string(msg.Key), string(msg.Value))
			case <-signals:
				fmt.Println("Interrupt is detected")
				doneCh <- struct{}{}
			}
		}
	}()
	<-doneCh
	fmt.Println("Processed", messageCountStart, "messages")
}

func PostMessage(res http.ResponseWriter, req *http.Request) {
	var evt event.Event
	dec := json.NewDecoder(req.Body)
	if err := dec.Decode(&evt); err != nil {
		log.Println(err)
	}

	if evt.UserID == "" {
		http.Error(res, "invalid user", 404)
	}

	eventable := event.NewEvent()
	eventable.UserID = evt.UserID
	eventable.Type = "post.event"
	data := map[string]interface{}{
		"data": evt,
	}
	eventable.Data = data
	if err := eventable.Send(eventable); err != nil {
		log.Panic(err)
	}

	res.WriteHeader(200)
	res.Write([]byte("OK"))
}

func main() {
	var dir string
	go consumerListener()

	flag.StringVar(&dir, "dir", ".", "the directory to serve files from. Defaults to the current dir")
	flag.Parse()
	r := mux.NewRouter()
	chClient := make(chan client.Client)
	hub := hub.NewHub()
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(dir))))
	r.HandleFunc("/api/notification", hub.Serve(chClient))
	r.HandleFunc("/message", PostMessage).Methods("POST")

	middleware := handlers.CORS(
		handlers.AllowCredentials(),
		handlers.AllowedMethods([]string{"GET"}),
		handlers.AllowedHeaders([]string{"*"}),
		handlers.AllowedOrigins([]string{"*"}),
	)

	if err := http.ListenAndServe(":3000", middleware(r)); err != nil {
		log.Fatal(err)
	} else {
		log.Printf("Websocket is serving on %s\n", ":3000")
	}
}
